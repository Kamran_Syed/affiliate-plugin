<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Product Name Here | Your Secret Admirer</title>
		<?php include_once('includes/head.php'); ?>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<!--Header-->
		<header class="inner-head">
			<?php include_once('includes/inner-nav.php'); ?>
		</header>
		<!--Featured Project sec-->
		<section class="blog-page-sec product-list-sec">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<ul class="breadcrumb">
							<li><a href="/">Home</a></li>
							<li><a href="/shop.php">Shop</a></li>
							<li><a href="#">Product Title Here</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="product-view">
							<div class="targetarea diffheight"><img id="multizoom2" alt="zoomable" title="" src="/img/pro-big01.jpg"/></div>
							<div class="multizoom2 thumbs"> <a href="img/pro-big01.jpg" data-large="img/pro-lg01.jpg" data-title="img"><img src="/img/pro-sm01.jpg" class="img-responsive" alt="img" title=""/></a> <a href="img/pro-big02.jpg" data-large="img/pro-lg02.jpg" data-title="img"><img src="/img/pro-sm02.jpg" class="img-responsive" alt="img" title=""/></a> <a href="img/pro-big03.jpg" data-large="img/pro-lg03.jpg" data-title="img"><img src="/img/pro-sm03.jpg" class="img-responsive" alt="img" title=""/></a> </div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="product-view-detail">
							<h2>The Rejuvenation Box</h2>
							<h3>$180</h3>
							<h4>In Stock</h4>
							<p>Suspendisse rhoncus, sapien in commodo ultrices, libero mauris molestie mauris, nec ultricies nisi dui eu ex. Donec non odio leo. Nullam at commodo nulla, sed tempor tellus. </p>
							<a href="#" class="buy-now">Buy Now</a>
							<div class="share-icon">
								<h5>Share this:</h5>
								<ul>
									<li><a href="#"><img src="/img/fb-icon.png" class="img-responsive" alt="img"></a></li>
									<li><a href="#"><img src="/img/tw-icon.png" class="img-responsive" alt="img"></a></li>
									<li><a href="#"><img src="/img/ins-icon.png" class="img-responsive" alt="img"></a></li>
									<li><a href="#"><img src="/img/pint-icon.png" class="img-responsive" alt="img"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="write-review">
							<h2 class="write-rvw-heading">Write a review</h2>
							<p>No review available. Be the first to write a review about this product</p>
							<a href="#" class="buy-now">Write a review</a> </div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h2 class="write-rvw-heading">Recomended Products</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<div class="recomended-pro">
							<div class="recomended-pro-img"> <img src="/img/rcm-img01.jpg" class="img-responsive" alt="img"> </div>
							<h3>The Gifty Gift Special</h3>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="recomended-pro">
							<div class="recomended-pro-img"> <img src="/img/rcm-img02.jpg" class="img-responsive" alt="img"> </div>
							<h3>The Gifty Gift Special</h3>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="recomended-pro">
							<div class="recomended-pro-img"> <img src="/img/rcm-img03.jpg" class="img-responsive" alt="img"> </div>
							<h3>The Gifty Gift Special</h3>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="recomended-pro">
							<div class="recomended-pro-img"> <img src="/img/rcm-img04.jpg" class="img-responsive" alt="img"> </div>
							<h3>The Gifty Gift Special</h3>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
	</body>
</html>