		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/multizoom.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet">
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700italic,700,800,800italic' rel='stylesheet' type='text/css'>
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/btt.css" rel="stylesheet">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/new-main.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/uploads/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/uploads/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/uploads/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/uploads/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/uploads/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/uploads/apple-touch-icon-152x152.png">
		<link rel="icon" type="image/png" href="/wp-content/uploads/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/wp-content/uploads/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="/wp-content/uploads/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/wp-content/uploads/manifest.json">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="/wp-content/uploads/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">