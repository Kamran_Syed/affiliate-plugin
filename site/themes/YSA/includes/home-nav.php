			<div id="navbar" >
				<?php
				$ysa_homenav_defaults = array(
					'theme_location'  => 'home',
					'menu'            => 'home',
					'container'       => false,
					'menu_class'      => 'nav navbar-nav menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 1,
				);
				wp_nav_menu( $ysa_homenav_defaults );
				?>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="top-bar"> <a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" class="img-responsive" alt="img"></a>
							<div class="menu-sec">
								<nav>
									<div class="navbar-header">
										<button type="button" class="navbar-toggle"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
										<a class="logo-sm" href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" class="img-responsive" alt="Your Secret Admirer Logo"></a> </div>
									<div  class="navbar-collapse collapse">
										<?php
										$ysa_homenav_defaults = array(
											'theme_location'  => 'home',
											'menu'            => 'home',
											'container'       => false,
											'menu_class'      => 'nav navbar-nav menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'depth'           => 1,
										);
										wp_nav_menu( $ysa_homenav_defaults );
										?>
									</div>
								</nav>
								<div class="cart-sec"> <span class="cart">
								<a href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cart-icon.png" class="img-responsive" alt="cart"></a><span><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span></span> </div>
							</div>
						</div>
