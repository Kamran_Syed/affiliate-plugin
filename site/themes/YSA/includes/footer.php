<footer>
	<a href="#top" class="cd-top">Top</a>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-social">
					<ul>
						<li><a href="http://facebook.com/yoursecretadmirerbox" ><i class="fa fa-facebook"></i></a></li>
						<li><a href="http://twitter.com/theadmirerbox"><i class="fa fa-twitter"></i></a></li>
						<li><a href="http://instagram.com/theadmirerbox"><i class="fa fa-instagram"></i></a></li>
						<li><a href="http://pinterest.com/admiresomeone"><i class="fa fa-pinterest-p"></i></a></li>
					</ul>
				</div>
				<div class="foot-links">
					<?php
					$ysa_botnav_defaults = array(
						'theme_location'  => 'bottom',
						'menu'            => 'bottom',
						'container'       => false,
						'menu_class'      => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'depth'           => 1,
					);
					wp_nav_menu( $ysa_botnav_defaults );
					?>
				</div>
				<div class="copy-right">&copy; COPYRIGHT Your Secret Admirer 2015. All Right Reserved.</div>
			</div>
		</div>
	</div>
</footer>


<!--JavaScript-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/btt.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/smooth-scroll.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script> 
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
<script>
	smoothScroll.init();
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/multizoom.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#image2').addimagezoom() // single image zoom with default options
		$('#multizoom2').addimagezoom({ // multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
			descArea: '#description2', // description selector (optional - but required if descriptions are used) - new
			disablewheel: false // even without variable zoom, mousewheel will not shift image position while mouse is over image (optional) - new

		});

	})

</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".custom-select").each(function(){
			jQuery(this).wrap("<span class='select-wrapper'></span>");
			jQuery(this).after("<span class='holder'></span>");
		});
		jQuery(".custom-select").change(function(){
			var selectedOption = jQuery(this).find(":selected").text();
			jQuery(this).next(".holder").text(selectedOption);
		}).trigger('change');
	})
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55b29a535c9f9fbb" async="async"></script>
<!-- Google Code for Remarketing Tag -->

<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
 -->
<script type="text/javascript">
var google_tag_params = {
dynx_itemid: 'REPLACE_WITH_VALUE',
dynx_itemid2: 'REPLACE_WITH_VALUE',
dynx_pagetype: 'REPLACE_WITH_VALUE',
dynx_totalvalue: 'REPLACE_WITH_VALUE',
};
</script>
<script type="text/javascript">
/*/ <![CDATA[ /
var google_conversion_id = 899560872;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/ ]]> /*/
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/899560872/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- begin olark code -->
<script type='text/javascript'>
window.olark||(function(k){var g=window,j=document,a=g.location.protocol=="https:"?"https:":"http:",i=k.name,b="load",h="addEventListener";(function(){g[i]=function(){(c.s=c.s||[]).push(arguments)};var c=g[i]._={},f=k.methods.length;while(f--){(function(l){g[i][l]=function(){g[i]("call",l,arguments)}})(k.methods[f])}c.l=k.loader;c.i=arguments.callee;c.p={0:+new Date};c.P=function(l){c.p[l]=new Date-c.p[0]};function e(){c.P(b);g[i](b)}g[h]?g[h](b,e,false):g.attachEvent("on"+b,e);c.P(1);var d=j.createElement("script"),m=document.getElementsByTagName("script")[0];d.type="text/javascript";d.async=true;d.src=a+"//"+c.l;m.parentNode.insertBefore(d,m);c.P(2)})()})({loader:(function(a){return "static.olark.com/jsclient/loader1.js?ts="+(a?a[1]:(+new Date))})(document.cookie.match(/olarkld=([0-9]+)/)),name:"olark",methods:["configure","extend","declare","identify"]});
// Please put your Olark Site ID here in place of the X's.
olark.identify('7147-468-10-7996');
</script>
<!-- end olark code -->
<?php wp_footer(); ?>