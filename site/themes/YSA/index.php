<?php get_header(); ?>
		<!--Header-->
		<header class="inner-head">
			<?php include_once('includes/inner-nav.php'); ?>
		</header>
		<!--Blog Post List sec-->
		<section class="blog-page-sec">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<p class="breadcrumb">
						<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
							yoast_breadcrumb();
						}
						?>
						</p>
						<?php
						if ( is_singular() ) {
						  // show adv. #1
						} else {
						  echo('<div class="page-title">
						  	<h1>Our Blog</h1>
						  	<h2>Your Secret Admirer</h2>
						  </div>');
						}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="blog-post">
							<div class="blog-pic"><a href="<?php the_permalink(); ?>"><?php 
							if ( has_post_thumbnail() ) { 
								the_post_thumbnail( '', array( 'class' => 'img-responsive' ) );
							} 
							?></a>
							<div class="date-sec"> <?php the_time( 'M' ); ?> <span><?php the_time( 'd' ); ?></span> <?php the_time( 'Y' ); ?> </div>
							</div>
							<a href="<?php the_permalink(); ?> " style="text-decoration:none;"><h2><?php the_title( ); ?></h2></a>
							<?php the_content( 'Read More&hellip;' ); ?>
							<?php comments_template(); ?>
						</div>
					<?php endwhile; else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
					<?php $pageinate_args = array(
						'base'               => '%_%',
						'format'             => '?page=%#%',
						'show_all'           => False,
						'end_size'           => 1,
						'mid_size'           => 2,
						'prev_next'          => True,
						'prev_text'          => __('Prev'),
						'next_text'          => __('Next'),
						'type'               => 'list',
						'add_args'           => False,
						'add_fragment'       => '',
						'before_page_number' => '',
						'after_page_number'  => ''
					); ?>
					<?php echo paginate_links( $pageinate_args ); ?>
					</div>
					<div class="col-sm-4">
						<div class="blog-sidebar">
							<?php dynamic_sidebar( 'blog_1' ); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?> 
	</body>
</html>