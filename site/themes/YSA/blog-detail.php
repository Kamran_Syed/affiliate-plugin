<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Blog Post Title Here</title>
		<?php include_once('includes/head.php') ;?>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<!--Header-->
		<header class="inner-head">
			<?php include_once('includes/inner-nav.php'); ?>
		</header>
		<!--Featured Project sec-->
		<section class="blog-page-sec">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<ul class="breadcrumb">
							<li><a href="#">Home</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Post</a></li>
						</ul>
						<div class="page-title">
							<h1>In The Box</h1>
							<h2>See Our latest News</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div class="blog-post">
							<div class="blog-pic"><img src="/img/blog-post1.jpg" class="img-responsive" alt="img">
								<div class="date-sec">
									May
									<span>09</span>
									2015
								</div>
							</div>
							<h2>Etiam molestie neque in enim dictum feugiat. Duis feugiat posuere.</h2>
							<p>Fusce eget massa at orci tempus vestibulum sed at eros. Vestibulum facilisis, ex eget fermentum mollis, urna orci vulputate est. Curabitur rutrum nisi magna, at suscipit nibh sollicitudin sed. Vivamus aliquet, dolor vel hendrerit tincidunt, tortor arcu facilisis erat, nec pharetra eros nisl tempor mauris. Quisque placerat nulla leo, at eleifend massa fermentum sit amet. </p>

							<p>Etiam egestas quis lacus id gravida. Vivamus sit amet consectetur tellus, sed tempus mi. Phasellus et felis vel metus cursus tristique et ut tellus. Maecenas mattis, dui ut sagittis porttitor, eros orci feugiat dolor, non rutrum dolor velit non massa. Etiam magna nibh, maximus sed magna id, tempus dapibus ipsum. Donec viverra nulla id tincidunt mollis. Sed eros metus, bibendum eget cursus ac, volutpat pharetra lectus. </p>

							<p>Aenean auctor tincidunt suscipit. Phasellus quis justo urna. Maecenas aliquet efficitur pellentesque. Nunc tincidunt diam hendrerit est efficitur, sit amet feugiat leo accumsan. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris eget erat a sem commodo scelerisque sit amet sit amet mauris. Nunc sed nibh eu nibh pellentesque consectetur. Maecenas orci tellus, semper in nisi ut, hendrerit ultrices neque. Duis sed enim congue, efficitur nibh ut, cursus tortor. Curabitur orci ante, commodo euismod ligula sed, faucibus imperdiet risus. Duis egestas est in erat interdum, in maximus lacus imperdiet. Suspendisse potenti. Ut efficitur nulla ac arcu aliquam interdum pulvinar eu dolor.</p>
							<h3>Leave a Comment</h3>
							<div class="blog-post-form">
								<ul>
									<li><input type="text" placeholder="Your name"></li>
									<li><input type="text" placeholder="Your Email"></li>
									<li><textarea placeholder="Message" class="message"></textarea></li>
									<li><input type="submit" value="Submit" class="blog-submit"></li>
								</ul>
							</div>
						</div>

					</div>
					<div class="col-sm-4">
						<div class="blog-sidebar">
							<h3>Subscribe To Our Newsletter</h3>
							<div class="newsletter-sec"><input type="text" placeholder="Enter Your Email Address"> <input type="submit" value="" class="newsletter"></div>
							<h3>Blog Categories</h3>
							<ul class="cat-list">
								<li><a href="#">The Gifty Gift  <span>(5)</span></a></li>
								<li><a href="#">Product launching   <span>(77)</span></a></li>
								<li><a href="#">The Hottest Events   <span>(6)</span></a></li>
								<li><a href="#">Tips &amp; Trick   <span>(11)</span></a></li>
								<li><a href="#">Latest News  <span>(54)</span></a></li>
								<li><a href="#">Popular Gifts   <span>(22)</span></a></li>
							</ul>
							<h3>Popular Products</h3>
							<ul class="popular-product">
								<li>
									<div class="pop-product-pic"><img src="/img/popular-product1.png" class="img-responsive" alt="img"></div>
									<div class="pop-product-des"><h2>The Gifty Gift Special</h2> <a href="#">View Details</a></div>
								</li>
								<li>
									<div class="pop-product-pic"><img src="/img/popular-product2.png" class="img-responsive" alt="img"></div>
									<div class="pop-product-des"><h2>The Gifty Gift Special</h2> <a href="#">View Details</a></div>
								</li>
								<li>
									<div class="pop-product-pic"><img src="/img/popular-product3.png" class="img-responsive" alt="img"></div>
									<div class="pop-product-des"><h2>The Gifty Gift Special</h2> <a href="#">View Details</a></div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
	</body>
</html>