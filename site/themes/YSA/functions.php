<?php

// Home Title Filter
add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title )
{
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		return __( get_bloginfo( 'name' ) ) . ' | ' . get_bloginfo( 'description' );
	}
	return $title;
}
// Feed Links
add_theme_support( 'automatic-feed-links' );

// Post Thumbnails
add_theme_support( 'post-thumbnails' );

//Nav
register_nav_menu( 'top', 'Top Menu' );
register_nav_menu( 'home', 'Home Menu' );
register_nav_menu( 'bottom', 'Botom Menu' );
// Nav Class
function add_menuclass($ulclass) {
	return preg_replace('/<a rel="data-scroll"/', '<a rel="data-scroll" data-scroll', $ulclass, -1);
}
add_filter('wp_nav_menu','add_menuclass');

// Widget Area
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Blog sidebar',
		'id'            => 'blog_1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

// PostTypes
function PostType_init() {
	$ysa_TestimonialLabels = array(
		'name' => _x('Testimonials', 'post type general name'),
		'singular_name' => _x('Testimonial', 'post type singular name'),
		'add_new' => _x('Add New', 'testimonial'),
		'add_new_item' => __('Add New Testimonial'),
		'edit_item' => __('Edit Testimonial'),
		'new_item' => __('New Testimonial'),
		'view_item' => __('View Testimonial'),
		'search_items' => __('Search Testimonial'),
		'not_found' =>  __('No testimonials found'),
		'not_found_in_trash' => __('No testimonial found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Testimonials'
	);
	$ysa_TestimonialArgs = array(
		'labels' => $ysa_TestimonialLabels,
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => 22,
		'menu_icon' => 'dashicons-format-chat',
		'rewrite' => array('slug' => 'testimonials','with_front' => false),
		'query_var' => true,
		'supports' => array('title','editor','thumbnail')
	);
	register_post_type('ysa_testimonial',$ysa_TestimonialArgs);
	$ysa_HomeSecLabels = array(
		'name' => _x('Home Sections', 'post type general name'),
		'singular_name' => _x('Home Section', 'post type singular name'),
		'add_new' => _x('Add New', 'home section'),
		'add_new_item' => __('Add New home section'),
		'edit_item' => __('Edit home section'),
		'new_item' => __('New home section'),
		'view_item' => __('View home'),
		'search_items' => __('Search home'),
		'not_found' =>  __('No home sections found'),
		'not_found_in_trash' => __('No home section found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Home Sections'
	);
	$ysa_HomeSecArgs = array(
		'labels' => $ysa_HomeSecLabels,
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-admin-home',
		'rewrite' => array('slug' => 'homesection','with_front' => false),
		'query_var' => true,
		'supports' => array('title','editor')
	);
	register_post_type('ysa_homesec',$ysa_HomeSecArgs);
}
add_action('init', 'PostType_init');

// PostType Title Filters
add_filter('gettext','custom_enter_title');
function custom_enter_title( $input ) {
	global $post_type;
	if( is_admin() && 'Enter title here' == $input && 'ysa_testimonial' == $post_type ) return 'Enter Testimonial Author Here';
	return $input;
}

// Custom Meta
add_action("admin_init", "admin_init");

function admin_init(){
	add_meta_box("credentials-meta", "Credentials", "credentials", "ysa_testimonial", "normal", "low");
	add_meta_box("position-meta", "Position", "position", "ysa_homesec", "normal", "low");
	add_meta_box("class-meta", "Section Class", "section_class", "ysa_homesec", "normal", "low");
}
function credentials(){
	global $post;
	$custom = get_post_custom($post->ID);
	$credentials = $custom["credentials"][0];
?>
<label>Credentials:</label>
<input type="text" id="credentials" name="credentials" value="<?php echo $credentials; ?>" size="70" />
<?php
}
function position(){
	global $post;
	$custom = get_post_custom($post->ID);
	$position = $custom["position"][0];
?>
<label>Position:</label>
<input type="number" id="position" name="position" value="<?php echo $position; ?>" />
<?php
}
function section_class(){
	global $post;
	$custom = get_post_custom($post->ID);
	$section_class = $custom["section_class"][0];
?>
<label>Section Class:</label>
<input type="text" id="section_class" name="section_class" value="<?php echo $section_class; ?>" />
<?php
}
add_action('save_post', 'save_details');
function save_details(){
	global $post;
	if(isset($_POST["credentials"])) update_post_meta($post->ID, "credentials", $_POST["credentials"]);
	if(isset($_POST["position"])) update_post_meta($post->ID, "position", $_POST["position"]);
	if(isset($_POST["section_class"])) update_post_meta($post->ID, "section_class", $_POST["section_class"]);
}

// WooCommerce
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
function woo_custom_cart_button_text() {
	return __( 'Buy Now', 'woocommerce' );
}


add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

	unset( $tabs['description'] ); 	// Remove the description tab
	return $tabs;
}

add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
	if ( is_user_logged_in() ) {
		$items .= '<li><a href="' . get_permalink( get_option('woocommerce_myaccount_page_id') ) . '">My Account</a></li>';
	}
	else {
		$items .= '<li><a href="' . get_permalink( get_option('woocommerce_myaccount_page_id') ) . '">Login</a></li>';
	}
	return $items;
}
// Remove Disqus from a custom post type
remove_action('pre_comment_on_post', 'dsq_pre_comment_on_post');
add_action( 'the_post' , 'block_disqus');
function block_disqus() {
	if ( get_post_type() == 'product' )
		remove_filter('comments_template', 'dsq_comments_template');
}
/**
 * woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );

/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package
 * @param array $package The package array/object being shipped
 * @return array of modified rates
 */
function hide_shipping_when_free_is_available( $rates, $package ) {

	// Only modify rates if free_shipping is present
	if ( isset( $rates['free_shipping'] ) ) {



		// To unset all methods except for free_shipping, do the following
		$free_shipping          = $rates['free_shipping'];
		$rates                  = array();
		$rates['free_shipping'] = $free_shipping;
	}

	return $rates;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
?>
<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

// Exclude Category from Shop Page
add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;

	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'boxes' ), // Don't display products in the boxes category on the shop page
			'operator' => 'NOT IN'
		)));

	}

	remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}

///Email template functionality starts from here

function set_html_content_type() {
	return 'text/html';
}

function show_mail_template(){
	ob_start();

?>

<div style = "width:auto;">

	<header id = "header" style = "border:1px solid black;background-color:#F4F4F3;"><b>Dear Admin,</b></header>

	<section>

		<article>

			<p>You have received a new message from your email address <a href="#link#">concierge@yoursecretadmirerbox.com</a></p>

			<p>Name: <b>**User name**</b></p>

			<p>E-mail Address : <b>**e-mail address**</b></p>

			<p>Contact Number : <b>**phone number**</b></p>

			<p>Budget : <b>**budget**</b></p>

			<p>Occasion :  <b>**occasion**</b></p>

			<p>Occasion Date : <b>**occsion_date**</b></p>
			<p>Card and Custom note : <b>**aspk_radio**</b></p>
			<p>Customize Your Gift : <b>**customize_your_gift**</b></p>
			<p>Categories : <b>**categories**</b></p>
			<p>Comments : <b>**comments**</b></p>
		</article>
		<h3>Thankyou </h3>
	</section>

</div>

<?php

	return ob_get_clean();

}
function aspk_handle_mail_template($email_template,$aspk_name,$aspk_email,$aspk_number,$aspk_occasion,$aspk_occsion_date,$aspk_radio,$customize,$category,$comments,$budget){
	if($customize){
		$customize_str = implode(",", $customize);
	} else{
		$customize_str = "No check applied";
	}
	if($category){
		$category_str = implode(",", $category);
	} else {
		$category_str = "No category selected";
	}
	if(!$aspk_number){
		$aspk_number = "No phone number";
	}
	$email_template = str_replace( '**User name**', ' '.$aspk_name , $email_template );

	$email_template = str_replace( '**e-mail address**', ' '.$aspk_email , $email_template );

	$email_template = str_replace( '**phone number**', ' '.$aspk_number , $email_template );

	$email_template = str_replace( '**budget**', ' '.$budget , $email_template );

	$email_template = str_replace( '**occasion**',' '.$aspk_occasion , $email_template );

	$email_template = str_replace( '**occsion_date**',' '.$aspk_occsion_date , $email_template );

	$email_template = str_replace( '**aspk_radio**', ' '.$aspk_radio , $email_template );

	$email_template = str_replace( '**customize_your_gift**', ' '.$customize_str , $email_template );
	$email_template = str_replace( '**categories**', ' '.$category_str , $email_template );
	$email_template = str_replace( '**comments**', ' '.$comments , $email_template );

	return $email_template;

}

function aspk_show_email_form(){
	if(isset($_POST['aspk_submit_email'])){
		$aspk_name=$_POST['aspk_name'];
		$aspk_email=$_POST['aspk_email'];
		$aspk_number=$_POST['aspk_number'];
		$budget=$_POST['budget'];
		$aspk_occasion=$_POST['aspk_occasion'];
		$aspk_occsion_date=$_POST['aspk_occsion_date'];
		$aspk_radio=$_POST['aspk_radio'];
		$customize=$_POST['customize'];
		$category=$_POST['category'];
		$comments=$_POST['comments'];
		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		$to = 'customize@yoursecretadmirerbox.com';
		$subject = 'New Custom Box Request';
		$email_template =show_mail_template();
		$email_template=aspk_handle_mail_template($email_template,$aspk_name,$aspk_email,$aspk_number,$aspk_occasion,$aspk_occsion_date,$aspk_radio,$customize,$category,$comments,$budget);
		wp_mail( $to, $subject, $email_template );
		// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
?>
<div class="col-md-12 digital_status" style="margin-top: 1em;margin-bottom:1em;background-color: #f0fcf5;padding: 0.5em;border: 1px solid #dbf7e7;color: #1e1e1e;text-align: center;font-weight: bold;font-size: larger;">
	Thank you! A member of our team will reach out to you shortly.
</div>
<?
	}
?>
<div class="tw-bs-container">
	<div class="row">
		<div class="col-sm-12">
			<div class="faq-custom"><h3>HOW IT WORKS</h3><div class="gap-20">&nbsp;</div><div class="text-center" style="font-size: 16px;"><p>Want to stand out even more? We can build customized boxes for any occasion right at our boutique, where we have many items that are not available on our website.</p><br><p>Our boutique carries products from small businesses, local artisans and women-owned businesses to give your gift box a unique feel.</p><br><p>And, for every gift box that we build this year, WE put together a care package for a child in need in the Philippines. You don’t need to pay anything extra – this is just our way of giving back. But YOU are helping make that happen.</p></div>
			</div>
		</div>
	</div>
	<div class="gap-20"></div>
	<div class="row">
		<div class="col-sm-5 hidden-xs" style="margin-top:50px;">
			<div class="work-img work-img-border">
				<img class="responsive-img" src="https://yoursecretadmirerbox.com/wp-content/uploads/2015/10/Customize.jpg" />
			</div>
		</div>
		<div class="col-sm-7" style="margin-top:1em;">
			<div class="custom text-center">
				<h3>Just fill out the following form and we’ll get back to you with 2 or 3 options to choose from!</h3>
				<p><em>*minimum of $35 budget</em></p>
			</div>
			<div class="gap-20"></div>
			<form method="post" action="">
				<div class="form-group ">
					<label class="control-label requiredField" for="aspk_name">
						Name
						<span class="asteriskField">
							*
						</span>
					</label>
					<input class="form-control" id="name" name="aspk_name" type="text" required>
				</div>
				<div class="form-group ">
					<label class="control-label requiredField" for="aspk_email">
						Email
						<span class="asteriskField">
							*
						</span>
					</label>
					<input class="form-control" id="email" name="aspk_email" type="email" required>
				</div>
				<div class="form-group ">
					<label class="control-label " for="number">
						Phone Number
					</label>
					<input class="form-control" id="number" name="aspk_number" type="text">
				</div>
				<div class="form-group ">
					<label class="control-label " for="budget">
						Budget (not including $10 packaging & shipping)
						<span class="asteriskField">
							*
						</span>
					</label>
					<input class="form-control" id="budget" name="budget" type="text" required>
				</div>
				<div class="form-group ">
					<label class="control-label ">
						Type of Occasion:
						<span class="asteriskField">
							*
						</span>
					</label>
					<input class="form-control" id="occasion" name="aspk_occasion" type="text" required>
				</div>
				<div class="form-group ">
					<label class="control-label " for="occsion_date">
						Date of Occasion:
						<span class="asteriskField">
							*
						</span>
					</label>
					<input class="form-control" id="occsion_date" name="aspk_occsion_date" type="date" required>
				</div>
				<div class="form-group ">
					<label class="control-label ">
						1. Do you want to include a card and custom note for $5?
					</label>
					<div class="">
						<div class="radio-inline">
							<label class="radio">
								<input name="aspk_radio" type="radio" value="Yes" checked>
								Yes
							</label>
						</div>
						<div class="radio-inline">
							<label class="radio">
								<input name="aspk_radio" type="radio" value="No">
								No
							</label>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label ">
						2. We want to make this gift special, so any insight into who the gift is for will help us customize your gift even further! Please check all that apply:
					</label>
					<div class="gap-20"></div>
					<div class="row-fluid">
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="feminine">
								feminine
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="masculine">
								masculine
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="colorful">
								colorful
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="monochromatic">
								monochromatic
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="chic">
								chic
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="outdoorsy">
								outdoorsy
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="crafty">
								crafty
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="mature">
								mature
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="youthful">
								youthful
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="romantic">
								romantic
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="professional">
								professional
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="customize[]" type="checkbox" value="funny">
								funny
							</label>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label class="control-label ">
						3. Please check off all of the categories that you are okay with putting in your box. We may not include items from all categories, but this will help to narrow down which sections we will select from.
					</label>
					<div class="gap-20"></div>
					<div class="row-fluid">
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Jewelry">
								jewelry
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Sweet Food">
								sweet food
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Savory Food">
								savory food
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Tableware">
								tableware
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Plants">
								plants
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Skincare &amp; Bath Products">
								skincare &amp; bath products
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Cosmetics ">
								cosmetics
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Clothing Accessories">
								clothing accessories
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Paper Products">
								paper products
							</label>
						</div>
						<div class="checkbox">
							<label class="checkbox">
								<input name="category[]" type="checkbox" value="Candles &amp; Home Fragrances">
								candles &amp; home fragrances
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="textarea">4. Additional comments</label>
					<textarea class="form-control" id="textarea" name="comments"></textarea>
				</div>
				<div class="form-group">
					<div class="gap-20"></div>
					<input class="the-box view-detail-custom" type="submit" value="Get Started" name="aspk_submit_email">
				</div>
			</form>
		</div>
	</div>
</div>
<?php
}

add_shortcode('show_email_form','aspk_show_email_form');
///Email template functionality ends

?>