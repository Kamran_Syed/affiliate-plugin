<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Your Secret Admirer :: Login</title>
		<?php include_once('includes/head.php'); ?>
	</head>
	<body>
		<!--Featured Project sec-->
		<section class="login-page-sec">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="login-content">
							<div class="login-logo"><a href="/"><img src="/img/logo.png" class="img-responsive" alt="Your Secret Admirer Large Logo"></a></div>
							<h1>ACCOUNT LOGIN</h1>
							<h3>Login with your email and password below.</h3>
							<ul class="login-form">
								<li><input type="text" placeholder="Email Address"></li>
								<li><input type="password" placeholder="Pasword"></li>
								<li><input type="submit" value="Login To Your Account" class="login-btn"></li>
								<li><fieldset class="checkboxes">
									<label class="label_check" for="checkbox-01"><input name="sample-checkbox-01" id="checkbox-01" value="1" type="checkbox"/> Remember Me</label>
									</fieldset> <a href="#">Forgot Password?</a></li>
							</ul>
							<h2>NEED HELP?</h2>
							<a href="mailto:Support@yoursecretadmirer.com" class="support-mail"><img src="img/mail-icon.png"  alt="img"> Support@yoursecretadmirer.com</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
		<script>
			var d = document;
			var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
			var gebtn = function(parEl,child) { return parEl.getElementsByTagName(child); };
			onload = function() {

				var body = gebtn(d,'body')[0];
				body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';

				if (!d.getElementById || !d.createTextNode) return;
				var ls = gebtn(d,'label');
				for (var i = 0; i < ls.length; i++) {
					var l = ls[i];
					if (l.className.indexOf('label_') == -1) continue;
					var inp = gebtn(l,'input')[0];
					if (l.className == 'label_check') {
						l.className = (safari && inp.checked == true || inp.checked) ? 'label_check c_on' : 'label_check c_off';
						l.onclick = check_it;
					};
					if (l.className == 'label_radio') {
						l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
						l.onclick = turn_radio;
					};
				};
			};
			var check_it = function() {
				var inp = gebtn(this,'input')[0];
				if (this.className == 'label_check c_off' || (!safari && inp.checked)) {
					this.className = 'label_check c_on';
					if (safari) inp.click();
				} else {
					this.className = 'label_check c_off';
					if (safari) inp.click();
				};
			};
			var turn_radio = function() {
				var inp = gebtn(this,'input')[0];
				if (this.className == 'label_radio r_off' || inp.checked) {
					var ls = gebtn(this.parentNode,'label');
					for (var i = 0; i < ls.length; i++) {
						var l = ls[i];
						if (l.className.indexOf('label_radio') == -1)  continue;
						l.className = 'label_radio r_off';
					};
					this.className = 'label_radio r_on';
					if (safari) inp.click();
				} else {
					this.className = 'label_radio r_off';
					if (safari) inp.click();
				};
			};
		</script>
	</body>
</html>