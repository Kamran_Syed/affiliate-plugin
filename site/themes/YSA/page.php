<?php get_header(); ?>
		<!--Header-->
		<header class="inner-head">
			<?php include_once('includes/inner-nav.php'); ?>
		</header>
		<!--Page sec-->
		<section class="page-sec">
			<div class="container">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title">
							<h1><?php the_title( ); ?></h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?php the_content( ); ?>
					</div>
				</div>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
	</body>
</html>