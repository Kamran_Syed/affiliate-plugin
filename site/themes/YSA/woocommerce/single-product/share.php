<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_share' ); // Sharing plugins can hook into here ?>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55b29a535c9f9fbb" async="async"></script>
<div class="share-icon">
	<h5>Share this:</h5>
<div class="addthis_sharing_toolbox"></div>
</div>