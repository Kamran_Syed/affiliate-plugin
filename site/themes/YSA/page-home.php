<?php get_header(); ?>
		<!--Header-->
		<header>
			<?php include_once('includes/home-nav.php'); ?>
			<?php
			    $args = array(
			    	'post_type'  => 'ysa_homesec',
			    	'meta_query' => array(
			    		array(
			    			'key'     => 'position',
			    			'value'   => '1',
			    		),
			    	),
			    );
			    $ysa_homesec = new WP_Query( $args );
			    if( $ysa_homesec->have_posts() ) {
			      while( $ysa_homesec->have_posts() ) {
			        $ysa_homesec->the_post();
			        ?>
			        	<div class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
		        				if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				            <?php the_content() ?>
			            </div>
			        <?php
			      }
			    }
			    else {
			      echo 'Sorry, no posts matched your criteria.';
			    }
			  ?>
					</div>
				</div>
			</div>
		</header>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '2',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '3',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '4',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<section class="what-people-say">
			<div class="container">
				<div class="row">
					<h2 class="content-heading">Testimonials</h2>
					<div class="col-sm-12">
						<div class="client-testimonial">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner" role="listbox">
								<?php
								    $testimonials = new WP_Query( 'post_type=ysa_testimonial' );
								    if( $testimonials->have_posts() ) {
								      while( $testimonials->have_posts() ) {
								        $testimonials->the_post();
								 ?>
							        <div class="item<?php if( $testimonials->current_post == 0 ) { echo ' active'; } ?>">
							        	<?php the_content(); ?>
  										<div class="client-detail">
  											<h3><?php the_title(); ?></h3>
  											<h4>
  											<?php
  												$credentials = get_post_meta( get_the_ID(), 'credentials', true );
  												if ( ! empty( $credentials ) ) {
  												    echo $credentials;
  												}
  											?>
  											</h4>
  										</div>
  										<div class="client-img">
  										<?php
  										if ( has_post_thumbnail() ) {
  											the_post_thumbnail( '', array( 'class' => 'img-responsive' ) );
  										}
  										?>
  										</div>
  									</div>
							    <?php } }
							    else {
							      echo 'Sorry, no posts matched your criteria.';
							    }
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Contant-->

		<section class="the-box">
			<div class="container">
				<div class="row">
					<h2 class="content-heading">Latest Posts</h2>
					<?php
						$args = array(
							'post_type'  => 'post',
						);
						$ysa_homesec = new WP_Query( $args );
						if( $ysa_homesec->have_posts() ) {
						  while( $ysa_homesec->have_posts() ) {
							$ysa_homesec->the_post();
					?>
						<div class="col-sm-4">
							<div class="blog-post">
								<div class="blog-pic"><a href="<?php the_permalink(); ?>"><?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( '', array( 'class' => 'img-responsive' ) );
								}
								?></a>
									<div class="date-sec"> <?php the_time( 'M' ); ?> <span><?php the_time( 'd' ); ?></span> <?php the_time( 'Y' ); ?> </div>
								</div>
								<a href="<?php the_permalink(); ?>" style="text-decoration:none;"><h2><?php the_title( ); ?></h2></a>
								<!--<p> //<?php the_excerpt( ); ?></p>-->
								<a href="<?php the_permalink(); ?>"><small>READ MORE...</small></a>
							</div>
						</div>
					<?php } }
						else {
							echo 'Sorry, no posts matched your criteria.';
						}
					?>
					<a href="<?php bloginfo('url'); ?>/our-blog" class="view-detail">View All</a> </div>
			</div>
		</section>
		<!--Contant-->

		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '7',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '8',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '9',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<?php
			$args = array(
				'post_type'  => 'ysa_homesec',
				'meta_query' => array(
					array(
						'key'     => 'position',
						'value'   => '10',
					),
				),
			);
			$ysa_homesec = new WP_Query( $args );
			if( $ysa_homesec->have_posts() ) {
			  while( $ysa_homesec->have_posts() ) {
				$ysa_homesec->the_post();
		?>
			<section class="<?php $section_class = get_post_meta( get_the_ID(), 'section_class', true );
					if ( ! empty( $section_class ) ) { echo $section_class; } ?>">
				<div class="container">
					<div class="row">
						<?php the_content() ?>
					</div>
				</div>
			</section>
		<?php } }
			else {
				echo 'Sorry, no posts matched your criteria.';
			}
		?>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
	</body>
</html>
