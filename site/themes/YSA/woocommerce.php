<?php get_header(); ?>
		<!--Header-->
		<header class="inner-head">
			<?php include_once('includes/inner-nav.php'); ?>
		</header>
		<!--Featured Project sec-->
		<section class="blog-page-sec product-list-sec">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<p class="breadcrumb">
						<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
							yoast_breadcrumb();
						}
						?>
						</p>
						<?php
						if ( is_singular() ) {
						  // show adv. #1
						} else {
						  // show adv. #2
						  echo('<div class="page-title">
						  	<h1>Product List</h1>
						  </div>');
						}
						?>
					</div>
				</div>
				<?php woocommerce_content(); ?>
			</div>
		</section>
		<!--Footer-->
		<?php include_once('includes/footer.php'); ?>
	</body>
</html>