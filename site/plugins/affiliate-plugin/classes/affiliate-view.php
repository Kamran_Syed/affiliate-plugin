<?php
if ( !class_exists('Aspk_Affiliate_Plugin_View')){
	class Aspk_Affiliate_Plugin_View{
		
		function __construct(){
			
			
		}
		
		function setting_affiliate(){
			$price = get_option( 'aspk_amount', 5);
			$selected_cats = get_option( 'aspk_affilaite_catg', array());
			
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<h1 style="text-align:center;">Affiliate Setting</h1>
					</div>
				</div>
				<form method="post" action="">
					<div class="row" style="clear:left;">
						<div class="col-md-12"> 
						<h3>Select Product Categories</h3>						
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-3" style="margin-top: 1em;">
							<b>Affiliate Price:</b>
						</div>
						<div class="col-md-9"><input type="text" name="aspk_aff_price" value="<?php echo $price; ?>"></div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-3" style="margin-top: 1em;"><b>Select Product Categories:</b></div>
						<div class="col-md-3" style="margin-top: 1em;">
							<select  class="multiselect form-control" multiple="multiple" name="aspk_product_cat[]" /required>
							<?php 
							$args = array(
								'number'     => $number,
								'orderby'    => $orderby,
								'order'      => $order,
								'hide_empty' => $hide_empty,
								'include'    => $ids
							);

							$product_categories = get_terms( 'product_cat', $args );
							
							foreach($product_categories as $catagory){
								$cat_id=$catagory->term_id;
								$cat_name=$catagory->name;
								?>
								<option value="<?php echo $cat_id; ?>" <?php if(in_array($cat_id,$selected_cats)) echo 'selected="selected"'; ?> ><?php echo $cat_name; ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
						&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top: 1em;">
							<input type="submit" name="submit_product_cat" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>
			<?php
		}
		
		function show_order_and_coupons($affiliate_orders,$aflt_order_lists){
		?>
			<div class="tw-bs container">
				<?php
						
				$uid = get_current_user_id();
				?>
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<div style="margin:5px;max-width:45em;"><strong>Affiliate URL:</strong><span style="border:1px solid;padding:5px;"> <?php echo home_url().'/?aspkid='.$uid; ?></span></div>
					</div>
				</div>
				
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
							<h2 style="">List of Affiliate orders</h2>
						</div>
					</div>
						
						<div class="row">
							<div class="col-md-3" style="margin-top:1em;">
								<b>Date/Time</b>
							</div>
							<div class="col-md-3" style="margin-top:1em;">
								<b>Order Number</b>
							</div>
							<div class="col-md-3" style="margin-top:1em;">
								<b>Awarded Amount</b>
							</div>
						</div>
					<?php
					if($affiliate_orders){
						
						foreach($affiliate_orders as $order){
							$price = $order->amount;
							?>
							<div class="row">
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $order->date_time;  ?>
								</div>
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $order->order_id;  ?>
								</div>
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $price; 
									
									?>
								</div>
							</div>
						<?php  
						}
						
						?>
							<form method="post" action="">
								<div class="row">
									<div class="col-md-12" style="margin-top: 1em;margin-bottom:2em;">
										<input type="submit" name="submit_coupons" value="Generate Coupon" class="btn btn-primary">
									</div>
								</div>
							</form>
						<?php
						
					}else{
						?>
						<div class="row">
							<div class="col-md-12" style="margin-top:1em;"><h3 style="color:green ;font-size: medium;margin-top: 1em;">Sorry, there is no affiliate order yet.</h3>
							</div>
						</div>
						<?php
					}  
					
					?>
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
							
							<h2 style="">List of Coupons</h2>
						</div>
					</div>
					<?php
					if($aflt_order_lists){
						?>
						<div class="row">
							<div class="col-md-3" style="margin-top:1em;">
								<b>Date/Time</b>
							</div>
							<div class="col-md-3" style="margin-top:1em;">
								<b>Coupon Code</b>
							</div>
							<div class="col-md-3" style="margin-top:1em;">
								<b>Coupon Amount</b>
							</div>
						</div>
					<?php
						foreach($aflt_order_lists as $list){
							?>
							<div class="row">
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $list->date_time; ?>
								</div>
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $list->coupon_name; ?>
								</div>
								<div class="col-md-3" style="margin-top:1em;">
									<?php echo $list->amount; ?>
								</div>
							</div>
					<?php 
						}
					
					}else{
						?>
						<div class="row">
							<div class="col-md-12" style="margin-top:1em;"><h3 style="color:green ;font-size: medium;margin-top: 1em;">Sorry, there are no Coupons yet.</h3>
							</div>
						</div>
						<?php
					}  
					?>
				
			</div>
			
			<?php
		}
		
	}// calss ends
}//if ends