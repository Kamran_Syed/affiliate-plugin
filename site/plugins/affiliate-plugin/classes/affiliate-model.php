<?php
if ( !class_exists('Aspk_Affiliate_Plugin_Model')){
	class Aspk_Affiliate_Plugin_Model{
		
		function __construct(){
			
			
		}
		
		function install_model(){
			global $wpdb;
			
				$sql="CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."affiliate_order` ( 
					`user_id` VARCHAR( 11 ) NOT NULL ,
					`order_id` VARCHAR( 11 ) NOT NULL ,
					`date_time` DATETIME  NULL ,
					`amount` VARCHAR( 255 )  NULL ,
					`status` VARCHAR( 11 ) NULL ,
					PRIMARY KEY (  `order_id` )
					) ENGINE = InnoDB;";
				$wpdb->query($sql);
				
				
				$sql="CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."affiliate_coupons` ( 
					`coupon_id` INT( 11 ) NOT NULL ,
					`coupon_name` VARCHAR( 11 ) NOT NULL ,
					`user_id` INT( 11 ) NOT NULL ,
					`date_time` DATETIME  NULL ,
					`amount` VARCHAR( 255 )  NULL ,
					PRIMARY KEY (  `coupon_id` )
					) ENGINE = InnoDB;";
				$wpdb->query($sql);
		}

		
		function is_coupon_unique($code){
			global $wpdb;
			
			$sql = "SELECT `ID` FROM {$wpdb->prefix}posts where `post_type` = 'shop_coupon' AND  `post_title` =  '{$code}'" ;
			$x = $wpdb->get_var($sql);	
			
			return $x;
		}
		
		function check_user_exist($id){
			global $wpdb;
			
			$sql = "SELECT `ID` FROM {$wpdb->prefix}users where `ID` = '{$id}'" ;
			return $wpdb->get_var($sql);
	
		}
		
		function get_posted_date($id){
			global $wpdb;
			
			$sql = "SELECT `post_date` FROM {$wpdb->prefix}posts where `ID` = '{$id}' AND `post_type` = 'shop_coupon' AND `post_content` = 'aspk'" ;
			return $wpdb->get_var($sql);
			
		}
		
		function get_coupon_content_and_id($name){
			global $wpdb;
			
			$sql = "SELECT `post_content` , `ID` FROM {$wpdb->prefix}posts where `post_name` = '{$name}' AND `post_type` = 'shop_coupon'" ;
			return $wpdb->get_row($sql);
			
		}
		
		function check_exist_afilated_order($id,$orderid){
			global $wpdb;
			
			$sql = "SELECT `user_id` FROM {$wpdb->prefix}affiliate_order where `user_id` = '{$id}' AND `order_id` = '{$orderid}'" ;
			return $wpdb->get_var($sql);
			
		}
		
		function insert_afilated_order($id,$orderid,$dt,$amount){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}affiliate_order (`user_id`,`order_id`,`date_time`,`amount`,`status`)  VALUES('{$id}','{$orderid}','{$dt}','{$amount}','notused')";
			$wpdb->query($sql);	
		}
		
		function insert_affiliate_coupons($id,$coupon_name,$user_id,$date_time,$amount){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}affiliate_coupons (`coupon_id`,`coupon_name`,`user_id`,`date_time`,`amount`)  VALUES('{$id}','{$coupon_name}','{$user_id}','{$date_time}','{$amount}')";
			$wpdb->query($sql);	
		}
		
		function get_affiliate_coupons_by_uid($id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}affiliate_coupons where `user_id` = '{$id}'" ;
			return $wpdb->get_results($sql);
			
		}
		
		function update_afilated_order($user_id){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}affiliate_order SET `status`='used' WHERE user_id ='{$user_id}'";
			$wpdb->query($sql);	
			
		}
		
		function get_affiliate_orders_by_user_id($user_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}affiliate_order` WHERE `user_id` = {$user_id} AND `status` = 'notused'";
			return $wpdb->get_results($sql);
			
		}
		
		function get_affiliate_orders_total($user_id){
			global $wpdb;
			
			$sql = "SELECT SUM(amount) FROM `{$wpdb->prefix}affiliate_order` WHERE `user_id` = {$user_id} AND `status` = 'notused'";
			
			return $wpdb->get_var($sql);
			
		}
		
		
		
		
	}// calss ends
}//if ends