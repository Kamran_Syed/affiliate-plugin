<?php

/**
 * Plugin Name: Affiliate Plugin
 * Plugin URI: 
 * Description: Affiliate Plugin
 * Version: 1.3
 * Author: Agile Solutions PK.
 * Author URI: http://agilesolutionspk.com
 */
 
require_once(__DIR__ ."/classes/affiliate-view.php");
require_once(__DIR__ ."/classes/affiliate-model.php");

 if ( !class_exists('Aspk_Affiliate_Plugin')){
	class Aspk_Affiliate_Plugin{
		
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') ); 
			add_action( 'woocommerce_before_cart', array(&$this,'apply_coupons' ));
			add_action( 'init', array(&$this,'fe_init' ));
			add_filter( 'woocommerce_coupon_is_valid',array(&$this,'filter_woocommerce_coupon_is_valid'), 10, 2 );
			add_action( 'woocommerce_order_status_processing',array(&$this, 'order_status_changed'));
			add_action('woocommerce_before_my_account', array(&$this, 'show_affiliate_order_list') );
			add_action( 'woocommerce_thankyou', array(&$this, 'show_affiliate_url') );

		}
		
		function show_affiliate_url($order_id){
			$order = new WC_Order($order_id);
			$uid = $order->get_user_id();
			
			if($uid == 0) return;
			
			?>
			<div class="row">
				<div class="col-md-12" style="margin-top:1em;">
					<div style="margin:5px;max-width:45em;"><strong>Affiliate URL:</strong><span style="border:1px solid;padding:5px;"> <?php echo home_url().'/?aspkid='.$uid; ?></span></div>
				</div>
			</div>
			<?php
		}
		
		function show_affiliate_order_list(){
			$this->show_affiliate_order();
			 
		}
		
		function order_status_changed( $order_id ) {
			global $woocommerce;
			
			$affiliate_m = new Aspk_Affiliate_Plugin_Model();
			
			$date_time = date('Y-m-d H:i:s');
			$order = new WC_Order($order_id);
			$coupons = $order->get_used_coupons();
			
			foreach($coupons as $c){
				$content = $affiliate_m->get_coupon_content_and_id($c);
				
				$coupon_id = $content->ID;
				$coupon_price = get_post_meta( $coupon_id, 'coupon_amount', true );
				
				$p_content = $content->post_content;
				$c_id =  explode(':',$p_content);
				$afilate_userid = $c_id[1];
				$rs_afid = $affiliate_m->check_exist_afilated_order($afilate_userid,$order_id);
				
				if(empty($rs_afid)){
					$rs_afid = $affiliate_m->insert_afilated_order($afilate_userid,$order_id,$date_time,$coupon_price);
				}
			}
			
		}
		
		function show_affiliate_order(){
			$affiliate_v = new Aspk_Affiliate_Plugin_View();
			$affiliate_m = new Aspk_Affiliate_Plugin_Model();
			
			$date_time = date('Y-m-d H:i:s');
			$user_id = get_current_user_id();
			$affiliate_orders = $affiliate_m->get_affiliate_orders_by_user_id($user_id);
			$aflt_order_lists = $affiliate_m->get_affiliate_coupons_by_uid($user_id);
			
			if(isset($_POST['submit_coupons'])){
				
				$order_amount = $affiliate_m->get_affiliate_orders_total($user_id);
				$product_catg = get_option('aspk_affilaite_catg', array());
				
				$aspk_coupon = $this->create_coupon($order_amount,$product_catg,$user_id);
				
				$coupon_id = $affiliate_m->is_coupon_unique($aspk_coupon);
				$affiliate_m->update_afilated_order($user_id);
				$affiliate_m->insert_affiliate_coupons($coupon_id,$aspk_coupon,$user_id,$date_time,$order_amount);
				
			}
			$affiliate_v->show_order_and_coupons($affiliate_orders,$aflt_order_lists);
			 
		}
		
		function filter_woocommerce_coupon_is_valid( $bool, $instance ){
			$aflt_model = new Aspk_Affiliate_Plugin_Model();
			// make filter magic happen here...
			
			$id = $instance->id;
			$dt = $aflt_model->get_posted_date($id);
			if($dt){
				$current = new DateTime();
				$current_time = $current->getTimestamp();
				
				$current_time_add = $current_time + 600;
				
				$datetime = new DateTime();
				$datetime->sub(new DateInterval('P1D'));
				$prev_datetime = $datetime->format('Y-m-d');
				
				if( $current_time > $current_time_add  ){
					update_post_meta( $new_coupon_id, 'expiry_date', $prev_datetime );
					return $bool;
				}
			}
			return $bool;
		}
		
		function randomnumber() {
			$alphabet = strtoupper("abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789");
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 10; $i++) {
			 $n = rand(0, $alphaLength);
			 $pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
		}
		
		function create_coupon($amount,$product_catg,$user_id){
			$aflt_model = new Aspk_Affiliate_Plugin_Model();
			
			do{
				$coupon_code = $this->randomnumber();
				
			}while( $aflt_model->is_coupon_unique( $coupon_code )  );
			
			$amount = $amount; // $amount
			$discount_type = 'fixed_cart'; // Type: fixed_cart, percent, fixed_product, percent_product
			$post_content = 'aspk:'.$user_id;		
			$coupon = array(
					'post_title' => $coupon_code,
					'post_content' => $post_content,
					'post_status' => 'publish',
					'post_author' => 1,
					'post_type'		=> 'shop_coupon'
				);
					
			$new_coupon_id = wp_insert_post( $coupon );
			
			// Add meta
			update_post_meta( $new_coupon_id, 'discount_type', $discount_type );
			update_post_meta( $new_coupon_id, 'coupon_amount', $amount );
			update_post_meta( $new_coupon_id, 'product_categories', $product_catg );
			update_post_meta( $new_coupon_id, 'expiry_date', date('Y-m-d', time()+86400) );
			
			return $coupon_code;
		}
		
		function fe_init(){
			if(! session_id()){
				session_start();
			}
			
			$affiliate_model = new Aspk_Affiliate_Plugin_Model();
			
			if(isset($_GET['aspkid'])){
				$userid = $_GET['aspkid'];
				
				$rs = $affiliate_model->check_user_exist($userid);
				if(!empty($rs)){
					$_SESSION['aspkid'] = $userid;
				}
			}
				
		}
		
		function apply_coupons() {
			global $woocommerce;
			
			if(isset($_SESSION['aspkid'])){
				$user_id = $_SESSION['aspkid'];
				if( !empty($user_id)){
					if(isset($_SESSION['aspk_coupon_code'])){
						$coupun_code = $_SESSION['aspk_coupon_code'];
					}else{
						$amount = get_option('aspk_amount', 5);
						$product_catg = get_option('aspk_affilaite_catg', array());
						$coupun_code = $this->create_coupon($amount,$product_catg, $user_id);  
						$_SESSION['aspk_coupon_code'] = $coupun_code;
						wc_print_notices();
						
						
					}
					
					$woocommerce->cart->add_discount( $coupun_code );
					
				}  
			}
		}

		
		function install(){
			$affiliate_m = new Aspk_Affiliate_Plugin_Model();
			
			$affiliate_m->install_model();
		}
		
		function admin_menu(){	
			add_menu_page('Affiliate Setting', 'Affiliate Setting', 'manage_options', 'aspk_affiliate_set', array(&$this, 'setting_affiliate') );
		}
		
		function setting_affiliate(){
			$affiliate_v = new Aspk_Affiliate_Plugin_View();
			
			if(isset($_POST['submit_product_cat'])){
				$product_cat = $_POST['aspk_product_cat'];
				$price = $_POST['aspk_aff_price'];
				update_option( 'aspk_affilaite_catg',$product_cat);
				update_option( 'aspk_amount',$price);
				echo '<div style="color:green ;font-size: medium;margin-top: 1em;">Settings have been saved</div>';
			}
			
			$affiliate_v->setting_affiliate();
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('aspk-aflt-js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__));
			wp_enqueue_style( 'aspk-aflt-agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
	}// calss ends
}//if ends

new Aspk_Affiliate_Plugin();